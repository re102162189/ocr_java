package com.tgl.ocr;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.regex.Matcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

/**
 * 
 * @author kitechen
 * 
 *         Implementation with Tesseract
 * 
 * @since 1.0
 * 
 */
public class ImgToText {

  private static final Logger LOG = LogManager.getLogger(ImgToText.class);

  private static final String CONFIG = "config.properties";

  private Tesseract tesseract;

  private boolean isIdMask = false;

  private String destPath = "";

  /**
   * <p>
   * initiate OCR - Tesseract
   * </p>
   * 
   * @param isIdMask ID masking flag
   * @param destPath Destination file path for output results
   */
  public ImgToText(boolean isIdMask, String destPath) {
    this.isIdMask = isIdMask;
    this.destPath = destPath;

    String lang = null;
    String dataPath = null;
    try (InputStream input = DocProcess.class.getClassLoader().getResourceAsStream(CONFIG)) {
      Properties prop = new Properties();
      prop.load(input);
      lang = prop.getProperty("data.lang");
      dataPath = prop.getProperty("data.path");
    } catch (IOException ex) {
      LOG.error(CONFIG, ex);
    }
    this.tesseract = new Tesseract();
    this.tesseract.setLanguage(lang);
    this.tesseract.setDatapath(dataPath);
    this.tesseract.setTessVariable("tessedit_create_pdf", "1");
    this.tesseract.setTessVariable("tessedit_pageseg_mode", "1");
  }

  /**
   * <p>
   * recursively travel file path
   * </p>
   * 
   * @param srcPath Root file path
   */
  public void filewalker(String srcPath) {
    File root = new File(srcPath);
    File[] list = root.listFiles((dir, name) -> !name.equals(".DS_Store"));
    if (list == null) {
      return;
    }

    for (File f : list) {
      if (f.isDirectory()) {
        filewalker(f.getAbsolutePath());
      } else {
        String content = null;
        try {
          content = tesseract.doOCR(f.getAbsoluteFile());
        } catch (TesseractException e) {
          LOG.error("tesseract OCR", e);
        }

        String fileName = f.getName();
        fileName = fileName.replace(".pdf", "");
        String ocrTextFile = destPath + fileName + ".txt";
        try (Writer out = new BufferedWriter(
            new OutputStreamWriter(new FileOutputStream(ocrTextFile), StandardCharsets.UTF_8))) {
          out.write(content);
          LOG.debug("file writen: {}", ocrTextFile);
          if (isIdMask) {
            ocrTextFile = destPath + fileName + "_encrpted.txt";
            String markedContent = replaceId(content);
            out.write(markedContent);
            LOG.debug("encrpted file writen: {}", ocrTextFile);
          }
        } catch (IOException e) {
          LOG.error("BufferedWriter", e);
        }
      }
    }
  }

  /**
   * <p>
   * check if any replace sensitive data
   * </p>
   * 
   * @param content The character sequence to be matched {@link ROCIdChecker#ID_BLUR_PATTERN}
   * @return text Convert masked id
   */
  private String replaceId(String content) {
    Matcher matcher = ROCIdChecker.ID_BLUR_PATTERN.matcher(content);
    StringBuffer sb = new StringBuffer();

    while (matcher.find()) {
      String id = matcher.group(0);
      if (ROCIdChecker.isValidIDorRCNumber(id)) {
        matcher.appendReplacement(sb, "@111111111");
      }
    }
    matcher.appendTail(sb);
    return sb.toString();
  }
}

package com.tgl.ocr;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author kitechen
 * 
 *         OCR program Main entry
 *
 * @since 1.0
 */
public class DocProcess {

  private static final Logger LOG = LogManager.getLogger(DocProcess.class);

  private static final String CONFIG = "config.properties";

  public static void main(String[] args) {

    String srcPath = null;
    String destPath = null;
    boolean isIdMask = false;

    try (InputStream input = DocProcess.class.getClassLoader().getResourceAsStream(CONFIG)) {
      Properties prop = new Properties();
      prop.load(input);
      srcPath = prop.getProperty("src.path");
      destPath = prop.getProperty("dest.path");
      isIdMask = Boolean.getBoolean(prop.getProperty("id.mask"));
    } catch (IOException e) {
      LOG.error(CONFIG, e);
    }

    Path path = Paths.get(destPath);
    if (!path.toFile().exists()) {
      try {
        Files.createDirectories(path);
      } catch (IOException e) {
        LOG.error("Files.createDirectories", e);
      }
    }

    Instant start = Instant.now();
    LOG.debug("================Processing Image To Text======================");
    ImgToText filewalker = new ImgToText(isIdMask, destPath);
    filewalker.filewalker(srcPath);
    Instant end = Instant.now();
    Duration duration = Duration.between(start, end);
    LOG.debug("Image to text elapsed: {} sec(s)", duration.getSeconds());
  }
}

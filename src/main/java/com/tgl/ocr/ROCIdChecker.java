package com.tgl.ocr;

import java.util.Arrays;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author kitechen
 * 
 *         verify TW id regulation
 * 
 * @since 1.0
 */
public final class ROCIdChecker {

  private static final Logger LOG = LogManager.getLogger(ROCIdChecker.class);

  /**
   * Blur ROC ID regex pattern, e.g. O/0, l/1, q/9
   */
  public static final Pattern ID_BLUR_PATTERN = Pattern.compile("[A-Z][0-9,O,l,q]{9}");
  
  /**
   * ROC ID regex pattern
   */
  private static final String ID_PATTERN = "[A-Z]{1}[1-2]{1}[0-9]{8}";
  
  /**
   * ROC Resident permit ID regex pattern
   */
  private static final String RESIDENT_PATTERN = "[A-Z]{1}[A-D]{1}[0-9]{8}";

  /**
   * 身分證第一碼
   */
  private static final char[] ID_CHAR = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
      'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

  /**
   * 原身分證英文字應轉換為10~33，這裡直接作個位數*9+10
   */
  private static final int[] ID_CODE = {1, 10, 19, 28, 37, 46, 55, 64, 39, 73, 82, 2, 11, 20, 48,
      29, 38, 47, 56, 65, 74, 83, 21, 3, 12, 30};

  /**
   * 原居留證第一碼英文字應轉換為10~33，十位數*1，個位數*9，這裡直接作[(十位數*1) mod 10] + [(個位數*9) mod 10]
   */
  private static final int[] ID_FIRST_CDOE =
      {1, 10, 9, 8, 7, 6, 5, 4, 9, 3, 2, 2, 11, 10, 8, 9, 8, 7, 6, 5, 4, 3, 11, 3, 12, 10};

  /**
   * 原居留證第二碼英文字應轉換為10~33，並僅取個位數*8，這裡直接取[(個位數*8) mod 10]
   */
  private static final int[] ID_SEC_CODE =
      {0, 8, 6, 4, 2, 0, 8, 6, 2, 4, 2, 0, 8, 6, 0, 4, 2, 0, 8, 6, 4, 2, 6, 0, 8, 4};

  private ROCIdChecker() {}

  /**
   * 
   * @param id Be matched by {@link #ID_BLUR_PATTERN}
   * @return ROC id validation
   */
  public static boolean isValidIDorRCNumber(String id) {

    if (id == null || "".equals(id)) {
      return false;
    }

    LOG.debug("replaced id before: {}", id);
    id = id.replaceAll("O", "0");
    if (id.startsWith("0")) {
      id = id.replaceFirst("0", "O");
    }
    id = id.replaceAll("l", "1");
    id = id.replaceAll("q", "9");
    LOG.debug("replaced id after: {}", id);

    id = id.toUpperCase();// 轉換大寫
    final char[] strArr = id.toCharArray();// 字串轉成char陣列
    int verifyNum = 0;

    /* 檢查身分證字號 */
    if (id.matches(ID_PATTERN)) {
      // 第一碼
      verifyNum = verifyNum + ID_CODE[Arrays.binarySearch(ID_CHAR, strArr[0])];
      // 第二~九碼
      for (int i = 1, j = 8; i < 9; i++, j--) {
        verifyNum += Character.digit(strArr[i], 10) * j;
      }
      // 檢查碼
      verifyNum = (10 - (verifyNum % 10)) % 10;
      return verifyNum == Character.digit(strArr[9], 10);
    }

    /* 檢查統一證(居留證)編號 */
    verifyNum = 0;
    if (id.matches(RESIDENT_PATTERN)) {
      // 第一碼
      verifyNum += ID_FIRST_CDOE[Arrays.binarySearch(ID_CHAR, strArr[0])];
      // 第二碼
      verifyNum += ID_SEC_CODE[Arrays.binarySearch(ID_CHAR, strArr[1])];
      // 第三~八碼
      for (int i = 2, j = 7; i < 9; i++, j--) {
        verifyNum += Character.digit(strArr[i], 10) * j;
      }
      // 檢查碼
      verifyNum = (10 - (verifyNum % 10)) % 10;
      return verifyNum == Character.digit(strArr[9], 10);
    }

    return false;
  }
}

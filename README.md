### Instructions
* setup config.properties, e.g. src.path, dest.path
* build command:  
	```
	mvn clean package
	```
* copy trained data in tesseract to assigned path (consist in config.properties)
* execute command:  
	```
	java -jar ocr-1.0.jar
	```

### Read Java Docs
* To extract the javadoc HTML files from the .jar file,  
	```
	jar -xvf ocr-1.0-javadoc.jar
	```
* see [Extracting Java Docs](https://docs.oracle.com/cd/E13187_01/JAM/v42/refrnce/javadocs.htm) 

### Implementation references
* http://tess4j.sourceforge.net/  
* https://github.com/tesseract-ocr/tessdata  
* https://gist.github.com/yyc1217/3856443